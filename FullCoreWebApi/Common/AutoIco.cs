﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Threading.Tasks;

namespace FullCoreWebApi.Common
{
    public class AutoIco
    {
        public static void AddAssembly(IServiceCollection service)
        {
            //var RuntimeHelper= AssemblyLoadContext.Default.LoadFromAssemblyName(new AssemblyName(assemblyName));
            ServiceLifetime serviceLifetime = ServiceLifetime.Singleton;
            //var types = RuntimeHelper.GetTypes();
            var assemblies = Assembly.Load("FullCoreRepository");
            var types = assemblies.GetTypes();
            var list = types.Where(t => t.IsClass && !t.IsAbstract && t.Name.Contains("pository")).ToList();
            foreach (var type in list)
            {
                service.AddTransient(type);
                // var interfaceList = type.GetInterfaces();
                //if (interfaceList.Any())
                //{
                //    var inter = interfaceList.First();
                //    switch (serviceLifetime)
                //    {
                //        case ServiceLifetime.Transient:
                //            service.AddTransient(inter, type);
                //            break;
                //        case ServiceLifetime.Scoped:
                //            service.AddScoped(inter, type);
                //            break;
                //        case ServiceLifetime.Singleton:
                //            service.AddSingleton(inter, type);
                //            break;
                //    }
                //}
            }
        }
    }
}
