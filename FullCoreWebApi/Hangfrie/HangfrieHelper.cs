﻿using Dapper;
using Hangfire;
using Hangfire.Dashboard;
using Hangfire.Dashboard.BasicAuthorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;


namespace HangFire.Common
{
    /// <summary>
    /// 
    /// </summary>
    public static class HangfrieHelper
    {
        public static BasicAuthAuthorizationFilter filter = new BasicAuthAuthorizationFilter(
              new BasicAuthAuthorizationFilterOptions
              {
                  SslRedirect = false,
                  // Require secure connection for dashboard
                  RequireSsl = false,
                  // Case sensitive login checking
                  LoginCaseSensitive = false,
                  // Users
                  Users = new[]
                  {
                        new BasicAuthAuthorizationUser
                        {
                            Login = "Admin",
                            // Password as plain text
                            PasswordClear = "123"
                        },
                      //new BasicAuthAuthorizationUser

                      //{
                      //    Login = "×××",//用户名
                      //    // Password as SHA1 hash
                      //    Password = new byte[]{ 0x54, ..., 0xa8 }//密码
                      //}
                  }
              });

        /// <summary>
        /// 加载定时任务
        /// </summary>
        /// <param name="backgroundJobs"></param>
        public static void Load(IBackgroundJobClient backgroundJobs)
        {

            //只一次性作业 反射方法加入队列
            //BindingFlags flag = BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod ;
            //List<string> MethodList= typeof(OnceWork).GetMethods(flag).Where(c=>c.ReturnType==typeof(void)&&c.GetParameters().Length==0).Select(c=>c.Name).ToList();
            //foreach (string me in MethodList)
            //{
            //    MethodCallExpression _methodCallexpP = Expression.Call(typeof(OnceWork).GetMethod(me));
            //    Expression<Action> _consStringExp = Expression.Lambda<Action>(_methodCallexpP);

            //    backgroundJobs.Enqueue(_consStringExp);
            //}
            //周期性作业
            // RecurringJob.AddOrUpdate(() => Console.WriteLine("1小时执行一次!"), Cron.Hourly);
            //参数说明 每天0点执行
            var queueName = "clockzero";
            RecurringJob.AddOrUpdate(
                () => TimesWork.AutoClearExtApiLlogs(), Cron.Daily(0, 0), TimeZoneInfo.Local, queueName
            );
            RecurringJob.AddOrUpdate(
               () => TimesWork.Test(), Cron.Daily(1, 0), TimeZoneInfo.Local, queueName
           );
            //只执行一次延迟作业
            // var jobId = BackgroundJob.Schedule(() => Console.WriteLine("2小时后执行"), TimeSpan.FromHours(2));
        }
    }
}
