using FullCoreWebApi.Common;
using FullCoreWebApi.Respository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Http;
using Hangfire;
using Hangfire.MemoryStorage;
using HangFire.Common;

namespace FullCoreWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHangfire(config => { config.UseMemoryStorage(); });
            AutoIco.AddAssembly(services);//依赖注入
            //services.AddMemoryCache();//使用服务器内存缓存
            //services.AddHostedService<MqttHostService>();//使用Mqtt消息队列
            //使用Newsoft josn序列化 配置
            services.AddControllersWithViews()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;//循环引用
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Include;//null值项包含
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver();//属性不自动转换规则
                }).AddJsonOptions(options => { options.JsonSerializerOptions.PropertyNamingPolicy = null; });//数据格式原样输出
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "FullCoreWebApi", Version = "v1" });
                c.OperationFilter<AddResponseHeadersFilter>();
                c.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
                c.OperationFilter<SecurityRequirementsOperationFilter>();
                //c.DocumentFilter<SwaggerDocTag>();//swaggerUI contrler名字中文对照
                //var basePath = Path.GetDirectoryName(typeof(Startup).Assembly.Location);
                //var xmlPath = Path.Combine(basePath, "FullCoreWebApi.xml");//Api Action 方法名对照注释
                //c.IncludeXmlComments(xmlPath);
                //var xmlPath2 = Path.Combine(basePath, "FullCoreControllers.xml");
                //c.IncludeXmlComments(xmlPath2);
            });
            services.AddMvc(options =>
            {
                //options.InputFormatters.Add(new PlainTextTypeFormatter());
                options.Filters.Add(typeof(ApiResultFilterAttribute));
                // options.Filters.Add(typeof(CustomExceptionAttribute));
                options.RespectBrowserAcceptHeader = true;
            });

            #region 注册JWT

          
            ////生成密钥
            //var script = "yuanfengchao123456789";
            //var keyByteArray = Encoding.UTF8.GetBytes(script);
            //var signingKey = new SymmetricSecurityKey(keyByteArray);

            ////认证参数
            //services.AddAuthentication("Bearer")
            //    .AddJwtBearer(o =>
            //    {
            //        o.TokenValidationParameters = new TokenValidationParameters
            //        {
            //            ValidateIssuer = true,//是否验证发行人，就是验证载荷中的Iss是否对应ValidIssuer参数
            //            ValidIssuer = "yfc",//发行人
            //            ValidateAudience = true,//是否验证订阅人，就是验证载荷中的Aud是否对应ValidAudience参数
            //            ValidAudience = "client",//订阅人

            //            ValidateIssuerSigningKey = true,//是否验证签名,不验证的画可以篡改数据，不安全
            //            IssuerSigningKey = signingKey,//解密的密钥                  
            //            ValidateLifetime = true,//是否验证过期时间，过期了就拒绝访问
            //            ClockSkew = TimeSpan.FromSeconds(1),//这个是缓冲过期时间，也就是说，即使我们配置了过期时间，这里也要考虑进去，过期时间+缓冲，默认好像是7分钟，你可以直接设置为0
                      
            //        };
            //    });
            #endregion

            //添加跨域规则
            //services.AddCors(setup =>
            //{
            //    setup.AddPolicy(name: "policy1", builder =>
            //    {
            //        builder.AllowAnyMethod()
            //            .AllowAnyHeader()
            //            .AllowAnyOrigin();
            //    });
            //});
            ConfigContext.Instance.Configuration = Configuration;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IBackgroundJobClient backgroundJobs)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "FullCoreWebApi v1");
                c.DefaultModelsExpandDepth(-1);//不显示Schemes  
            }
            );
            app.UseRouting();
            //允许访问静态页
            app.UseStaticFiles();
            //System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("zh-CN");
          
            app.UseHangfireDashboard("/bgworks", new DashboardOptions
            {
                Authorization = new[] { HangfrieHelper.filter },
                DisplayStorageConnectionString=false,
                DashboardTitle="急诊后台定时任务"
            });
            app.UseHangfireServer();


            //app.UseCors("policy1");//支持跨域
            //app.UseCustomExceptionMiddleware();
            //app.UseAuthentication();
            //app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapGet("/", async context =>
                //{
                //    context.Response.ContentType = "text/html;charset=utf-8";
                //    await context.Response.WriteAsync($"<span>急诊api<span/><br/><a href=\"swagger/index.html\">swaggerUI<a>");
                //});
               
                endpoints.MapControllerRoute(
                   name: "default",
                   pattern: "swagger/index.html");
               // endpoints.MapControllers();
            });
            HangfrieHelper.Load(backgroundJobs);
        }
    }
}
