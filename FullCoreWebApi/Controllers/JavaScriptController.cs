﻿using FullCoreWebApi.Respository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jint;
using FullCoreWebApi.Common;
using Newtonsoft.Json;
using FullCoreRepository;
using FullCoreDBModels;
using FullCoreControllers;

namespace FullCoreWebApi
{


    /// <summary>
    /// JavaScript 解释执行器
    /// </summary>
    public class JavaScriptController : APIControllerRoot
    {
        Jint.Engine engine;
        OracleRespository _OracleRespository;
        /// <summary>
        /// JavaScript 解释 构造方法
        /// </summary>
        /// <param name="OracleRespository"></param>
        public JavaScriptController(OracleRespository OracleRespository)
        {
            _OracleRespository = OracleRespository;
            engine = new Engine();
            engine.SetValue("NLog", new Action<object>(NLog));//记录日志
            engine.SetValue("OrcleSqlQuery", new Func<string, string, string>(OracleSqlQuery));//查询oracle数据库
        }
        #region 注册方法

        /// <summary>
        /// 添加日志
        /// </summary>
        /// <param name="msg"></param>
        private void NLog(object msg)
        {
            LogerHelper.GetLog().Info(msg.ToString());
        }

        /// <summary>
        /// 查询oralce 数据库
        /// </summary>
        /// <param name="connKey">数据库链接</param>
        /// <param name="sql">sql语句</param>
        /// <returns></returns>
        private string OracleSqlQuery(string connKey, string sql)
        {
            try
            {
                var data = _OracleRespository.OracleQuery(connKey, sql);
                return JsonConvert.SerializeObject(data);
            }
            catch (Exception e)
            {
                return SerialzeError(e.Message);
            }

        }

        /// <summary>
        /// pgsql查询数据
        /// </summary>
        /// <param name="connKey">数据库链接</param>
        /// <param name="sql">sql语句</param>
        private string PgSqlQuery(string connKey, string sql)
        {
            try
            {
                var data = _OracleRespository.OracleQuery(connKey, sql);
                return JsonConvert.SerializeObject(data);
            }
            catch (Exception e)
            {
                return SerialzeError(e.Message);
            }

        }
        private string SerialzeError(string errmsg)
        {
            var result = new CommonResult() { Result = false, ErrMsg = errmsg };
            return JsonConvert.SerializeObject(result);
        }

        #endregion

        /// <summary>
        /// Post 转换数据
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
        [HttpPost]
        
        public string Convert([FromBody] ScriptContent script)
        {
            try
            {
                engine.Execute(script.JavaScriptCode);
                var result = engine.Invoke(script.FunctionName, script.Data);
                return result.AsString();
            }
            catch (Exception ex)
            {
                LogerHelper.GetLog().Error(ex.Message);
                throw new Exception($"转换错误【{ex.Message}】", ex);
            }
        }
    }
}
