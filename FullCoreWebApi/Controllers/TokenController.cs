﻿using FullCoreControllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystemAPi.JWT;

namespace FullCoreWebApi.Controllers
{
 
    public class TokenController : APIControllerRoot
    {
        public  TokenController( )
        { 
        
        }
        [HttpGet]
        [AllowAnonymous]
        public string GetToken(string Name,string Value) 
        {
            string token = new JwtHelper().GenerateToken(Name,Value);
            return token;
        }
    }
}
