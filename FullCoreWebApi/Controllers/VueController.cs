﻿using FullCoreDBModels;
using FullCoreWebApi.VueModdel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Speech.AudioFormat;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using Antlr;
namespace FullCoreWebApi.Controllers
{
    /// <summary>
    /// Vue项目的接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class VueController : ControllerBase
    {
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
        [HttpPost]

        public async Task<CommonResult> Login([FromBody] UserInfo script)
        {
            CommonResult res = new CommonResult() { Result = true, ReturnData = "", ErrMsg = "成功" };
            try
            {
                await Task.Delay(3000);
                return res;
            }
            catch (Exception ex)
            {
                return res;
            }
        }


        //public async Task<CommonResult> ConvertTempStr([FromBody] string script)
        //{
        //    CommonResult res = new CommonResult() { Result = true, ReturnData = "", ErrMsg = "成功" };
        //    try
        //    {
        //        dynamic PatModel = JsonConvert.DeserializeObject<ExpandoObject>(script);
        //        //  dynamic param = new System.Dynamic.ExpandoObject();
        //        // PatModel.CallList = new List<CallRecord>() { };

        //        string TempStr = "";
        //        TempStr += "$PatModel.Pat1$  || $PatModel.Pat2$ \r\n";
        //        TempStr += "$PatModel.Base.Base1$  || $PatModel.Base.Base2$ \r\n";
        //        TempStr += "<table>$PatModel.CallList:{<tr class=black><td>$it.Call1$</td></tr> <tr class=red><td>$it.Call2$</td></tr>}$</table>";
        //        //Antlr.StringTemplate st = new StringTemplate(TempStr);
        //        //st.SetAttribute("PatModel", PatModel);
        //        //return st.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        return res;
        //    }
        //}



    }
}
