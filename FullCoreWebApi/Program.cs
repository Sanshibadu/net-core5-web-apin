using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullCoreWebApi
{
    /// <summary>
    /// 
    /// </summary>
    public class Program
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            var builder = CreateHostBuilder(args);
            CreateHostBuilder(args).Build().Run();
            //ʹ��autoFac
            //builder.ConfigureContainer<ContainerBuilder>(builder =>
            //{
            //    var assemblies = Assembly.Load("Repository");
            //    builder.RegisterAssemblyTypes(assemblies)
            //        .Where(t => t.IsClass && !t.IsAbstract && t.Name.Contains("Repository"))
            //        .AsSelf()
            //        .InstancePerDependency();
            //});
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                  .ConfigureWebHostDefaults(webBuilder =>
                  {
                      webBuilder.UseStartup<Startup>();
                  }).UseNLog();
        }
        //.UseEnvironment(Microsoft.AspNetCore.Hosting.EnvironmentName.Development)
    }
}
