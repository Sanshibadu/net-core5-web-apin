﻿using Dapper;
using FullCoreDBModels;
using FullCoreRepository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FullCoreWebApi.Respository
{
    public class PatientInfoRepository
    {
        public PatientInfoRepository()
        {
            var jsonFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "JsonSql", "PatInfo.json");
            var JsonSqls = JsonConvert.DeserializeObject<List<JsonSql>>(File.ReadAllText(jsonFile));
            if (JsonSqls.Any())
            {
                SelectPatInfoListSql = JsonSqls.FirstOrDefault(c => c.SqlKey.Equals("SelectPatInfoList"))?.SqlValue;
            }
            LogerHelper.GetLog().Info("加载jsonsql数据");
        }

        private string SelectPatInfoListSql;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<PatientInfo> GetPatientInfoList()
        {
            try
            {
                using (var conn = DapperFactory.GetDefaultConnation())
                {
                    return conn.Query<PatientInfo>(SelectPatInfoListSql).ToList();
                }
            }
            catch (Exception)
            {
                //日志记录
            }
            return new List<PatientInfo>();
        }

        /// <summary>
        /// 获取患者信息
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        public PatientInfo GetPatientInfo(string parms)
        {
            try
            {
                string sql = $"select * from triage_patientvisit tp where tp.patientid='{parms}'";
                using (var conn = DapperFactory.GetDefaultConnation())
                {
                    return conn.Query<PatientInfo>(sql).FirstOrDefault();
                }
            }
            catch (Exception)
            {
                //日志记录
            }
            return new PatientInfo();
        }
    }
}
