﻿
using FullCoreDBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FullCoreRepository
{
    public class ConditionArgsHelper
    {
        public static string JoinSQLStr(params ConditionArgs[] args)
        {
            try
            {
                if (args == null || args.Length == 0)
                {
                    return string.Empty;
                }

                StringBuilder retString = new StringBuilder();
                ConditionArgs lastcondition = null;
                foreach (ConditionArgs item in args)
                {
                    List<string> keys = null;
                    if (item.Key is System.Collections.IList)
                    {
                        keys = item.Key as List<string>;
                    }
                    else
                    {
                        keys = new List<string>() { item.Key.ToString() };
                    }

                    if (keys == null || string.IsNullOrEmpty(keys.ToString()))
                        continue;
                    //var keys = item.Key.Split(',');
                    if (retString.Length != 0)
                    {
                        switch (item.PreConnector)
                        {
                            case ConnectorEnum.And:
                                if (lastcondition != null && lastcondition.PreConnector == ConnectorEnum.Or)
                                {
                                    retString = new StringBuilder(string.Format("({0})", retString));
                                }
                                retString.Append(" AND ");
                                break;

                            case ConnectorEnum.Or:
                                retString = new StringBuilder(string.Format("({0})", retString) + " OR ");
                                break;
                        }
                    }
                    var condition = new StringBuilder();
                    foreach (var keyItem in keys)
                    {
                        Type t;

                        var midConn = string.Empty;
                        switch (item.MidConnector)
                        {
                            case ConnectorEnum.And:
                                midConn = " AND ";
                                break;

                            case ConnectorEnum.Or:
                                midConn = " OR ";
                                break;
                        }
                        if (condition.Length != 0)
                        {
                            condition.Append(midConn);
                        }

                        string itemVal;
                        if (item.Value == null)
                        {
                            itemVal = "NULL";
                        }
                        else
                        {
                            t = item.Value.GetType();
                            if (t == typeof(DateTime))
                            {
                                itemVal = Convert.ToDateTime(item.Value).ToString("yyyy-MM-dd HH:mm:ss.fff"); ;
                            }
                            else if (t == typeof(bool))
                            {
                                itemVal = Convert.ToInt32(Convert.ToBoolean(item.Value)).ToString();
                            }
                            else
                            {
                                itemVal = item.Value.ToString();
                            }
                        }

                        switch (item.JudgeType)
                        {
                            #region
                            case JudgmentEnum.Equal:
                                if (item.Value is System.Collections.IList)
                                {
                                    var val = item.Value as List<string>;
                                    var str = string.Format("' {0} {1} = N'", midConn, keyItem);
                                    condition.AppendFormat(" ( {0} = N'{1}' ) ", keyItem, string.Join(str, val));
                                }
                                else if (item.Value == null)
                                {
                                    condition.AppendFormat(" {0} = {1}", keyItem, itemVal);
                                }
                                else
                                {
                                    condition.AppendFormat(" {0} = N'{1}'", keyItem, itemVal);
                                }
                                break;

                            case JudgmentEnum.UnEqual:
                                if (item.Value is System.Collections.IList)
                                {
                                    var val = item.Value as List<string>;
                                    var str = string.Format("' {0} {1} <> N'", midConn, keyItem);
                                    condition.AppendFormat(" ( {0} <> N'{1}' ) ", keyItem, string.Join(str, val));
                                }
                                else if (item.Value == null)
                                {
                                    condition.AppendFormat(" {0} <> {1}", keyItem, itemVal);
                                }
                                else
                                {
                                    condition.AppendFormat(" {0} <> N'{1}'", keyItem, itemVal);
                                }
                                break;

                            case JudgmentEnum.GreaterThan:
                                if (item.Value is System.Collections.IList)
                                {
                                    var val = item.Value as List<string>;
                                    var str = string.Format("' {0} {1} > N'", midConn, keyItem);
                                    condition.AppendFormat(" ( {0} > N'{1}' ) ", keyItem, string.Join(str, val));
                                }
                                else if (item.Value == null)
                                {
                                    condition.AppendFormat(" {0} > {1}", keyItem, itemVal);
                                }
                                else
                                {
                                    condition.AppendFormat(" {0} > N'{1}'", keyItem, itemVal);
                                }
                                break;

                            case JudgmentEnum.EqualOrGreaterThan:
                                if (item.Value is System.Collections.IList)
                                {
                                    var val = item.Value as List<string>;
                                    var str = string.Format("' {0} {1} >= N'", midConn, keyItem);
                                    condition.AppendFormat(" ( {0} >= N'{1}' ) ", keyItem, string.Join(str, val));
                                }
                                else if (item.Value == null)
                                {
                                    condition.AppendFormat(" {0} >= {1}", keyItem, itemVal);
                                }
                                else
                                {
                                    condition.AppendFormat(" {0} >= N'{1}'", keyItem, itemVal);
                                }
                                break;

                            case JudgmentEnum.LessThan:
                                if (item.Value is System.Collections.IList)
                                {
                                    var val = item.Value as List<string>;
                                    var str = string.Format("' {0} {1} < N'", midConn, keyItem);
                                    condition.AppendFormat(" ( {0} < N'{1}' ) ", keyItem, string.Join(str, val));
                                }
                                else if (item.Value == null)
                                {
                                    condition.AppendFormat(" {0} < {1}", keyItem, itemVal);
                                }
                                else
                                {
                                    condition.AppendFormat(" {0} < N'{1}'", keyItem, itemVal);
                                }
                                break;

                            case JudgmentEnum.EqualOrLessThan:
                                if (item.Value is System.Collections.IList)
                                {
                                    var val = item.Value as List<string>;
                                    var str = string.Format("' {0} {1} <= N'", midConn, keyItem);
                                    condition.AppendFormat(" ( {0} <= N'{1}' ) ", keyItem, string.Join(str, val));
                                }
                                else if (item.Value == null)
                                {
                                    condition.AppendFormat(" {0} <= {1}", keyItem, itemVal);
                                }
                                else
                                {
                                    condition.AppendFormat(" {0} <= N'{1}'", keyItem, itemVal);
                                }
                                break;

                            case JudgmentEnum.LikeAll:
                                if (item.Value is System.Collections.IList)
                                {
                                    var val = item.Value as List<string>;
                                    var str = string.Format("%' {0} {1} LIKE N'%", midConn, keyItem);
                                    condition.AppendFormat(" ( {0} LIKE N'%{1}%' ) ", keyItem, string.Join(str, val));
                                }
                                else
                                {
                                    condition.AppendFormat(" {0} LIKE N'%{1}%'", keyItem, itemVal);
                                }
                                break;

                            case JudgmentEnum.LikeLeft:
                                if (item.Value is System.Collections.IList)
                                {
                                    var val = item.Value as List<string>;
                                    var str = string.Format("' {0} {1} LIKE N'%", midConn, keyItem);
                                    condition.AppendFormat(" ( {0} LIKE N'%{1}' ) ", keyItem, string.Join(str, val));
                                }
                                else
                                {
                                    condition.AppendFormat(" {0} LIKE N'%{1}'", keyItem, itemVal);
                                }
                                break;

                            case JudgmentEnum.LikeRight:
                                if (item.Value is System.Collections.IList)
                                {
                                    var val = item.Value as List<string>;
                                    var str = string.Format("%' {0} {1} LIKE N'", midConn, keyItem);
                                    condition.AppendFormat(" ( {0} LIKE N'{1}%' ) ", keyItem, string.Join(str, val));
                                }
                                else
                                {
                                    condition.AppendFormat(" {0} LIKE N'{1}%'", keyItem, itemVal);
                                }
                                break;

                            case JudgmentEnum.In:
                                if (item.Value is System.Collections.IList)
                                {
                                    var val = item.Value as List<string>;
                                    condition.AppendFormat(" {0} IN({1})", keyItem, string.Format("N'{0}'", string.Join("',N'", val)));
                                }
                                else if (item.Value == null)
                                {
                                    condition.AppendFormat(" {0} = {1} ", keyItem, itemVal);
                                }
                                else
                                {
                                    condition.AppendFormat(" {0} = N'{1}'", keyItem, item.Value);
                                }
                                break;

                            case JudgmentEnum.NotIn:
                                if (item.Value is System.Collections.IList)
                                {
                                    var val = item.Value as List<string>;
                                    condition.AppendFormat(" {0} NOT IN ({1})", keyItem, string.Format("N'{0}'", string.Join("',N'", val)));
                                }
                                else if (item.Value == null)
                                {
                                    condition.AppendFormat(" {0} <> {1} ", keyItem, itemVal);
                                }
                                else
                                {
                                    condition.AppendFormat(" {0} <> ( N'{1}')", keyItem, item.Value);
                                }
                                break;
                                #endregion
                        }
                    }
                    lastcondition = item;
                    if (keys.Count > 1)
                    {
                        retString.AppendFormat(" ({0}) ", condition);
                    }
                    else
                    {
                        retString.Append(condition);
                    }
                }

                if (string.IsNullOrEmpty(retString.ToString()))
                {
                    return string.Empty;
                }
                else
                {
                    return string.Format(" AND {0}", retString);
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        /// 
        /// 获取类中的属性值
        /// 
        public static Object GetEntityValue(string PropertyName, object obj)
        {
            try
            {
                Type Ts = obj.GetType();
                object o = Ts.GetProperty(PropertyName).GetValue(obj, null);
                //DateTime temp = DateTime.MinValue;
                //if (DateTime.TryParse(Convert.ToString(o), out temp))
                //{
                //    o = string.Format("TO_DATE('{0}','yyyy-mm-dd HH24:mi:ss')", o);
                //}
                return o;
            }
            catch
            {
                return PropertyName;
            }
        }
    }
}
