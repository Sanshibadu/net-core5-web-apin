﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FullCoreWebApi.Respository
{
    public class ConfigContext
    {
        /// <summary>
        /// 延迟加载对象
        /// </summary>
        private static readonly Lazy<ConfigContext> _lazy = new Lazy<ConfigContext>(() => new ConfigContext(), LazyThreadSafetyMode.PublicationOnly);

        /// <summary>
        /// 单例对象
        /// </summary>
        public static ConfigContext Instance => _lazy.Value;

        public IConfiguration Configuration { get; set; }


        private ConfigContext()
        {

        }
    }
}
