﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System.Data.Odbc;
using Npgsql;
using FullCoreWebApi.Respository;

namespace FullCoreRepository
{
    public static class DapperFactory
    {
        /// <summary>
        /// 主API数据库连接
        /// </summary>
        private static string ConnStr = "";
        /// <summary>
        /// 主API数据库连接类型
        /// </summary>
        private static string ConnType = "";
        /// <summary>
        /// 其他数据库连接
        /// </summary>
        private static Dictionary<string, string> DictConnString = new Dictionary<string, string>();
        static DapperFactory()
        {
            ConnStr = ConfigContext.Instance.Configuration.GetConnectionString("PAT1");
            ConnType = ConfigContext.Instance.Configuration.GetSection("ConnType").Value;
            var child = ConfigContext.Instance.Configuration.GetSection("OtherConnList").GetChildren();
            foreach (var item in child)
            {
                DictConnString.Add(item.Key, item.Value);
            }
        }

        public static IDbConnection GetDefaultConnation()
        {
            switch (ConnType)
            {
                case "oracle":
                    return CreateOracleConnection();
                //case "odbc":
                //    return CreateOdbcConnection();
                case "pgsql":
                    return CreatePgsqlConnection();
                default:
                    break;
            }
            return null;
        }
        public static IDbConnection CreateDefaultConnection()
        {
            return GetDefaultConnation();
        }

        /// <summary>
        /// 创建oracle数据库连接
        /// </summary>
        /// <param name="connKey"></param>
        /// <param name="connType"></param>
        /// <returns></returns>
        public static IDbConnection CreateOracleConnection(string connKey = null)
        {
            string conn = !string.IsNullOrEmpty(connKey) ? ConfigContext.Instance.Configuration.GetConnectionString(connKey): ConnStr;
            var dbconn = new OracleConnection(conn);
            dbconn.Open();
            return dbconn;
        }

        /// <summary>
        /// 创建odbc数据库连接
        /// </summary>
        /// <param name="connKey"></param>
        /// <param name="connType"></param>
        /// <returns></returns>
        //public static IDbConnection CreateOdbcConnection(string connKey = null)
        //{
        //    string conn = !string.IsNullOrEmpty(connKey) ? ConfigContext.Instance.Configuration.GetConnectionString(connKey) : ConnStr;
        //    var dbconn = new OdbcConnection(conn);
        //    dbconn.Open();
        //    return dbconn;
        //}

        /// <summary>
        /// 创建Pgsql据库连接
        /// </summary>
        /// <param name="connKey"></param>
        /// <param name="connType"></param>
        /// <returns></returns>
        public static IDbConnection CreatePgsqlConnection(string connKey = null)
        {
            string conn = !string.IsNullOrEmpty(connKey) ? ConfigContext.Instance.Configuration.GetConnectionString(connKey) : ConnStr;
            var dbconn = new NpgsqlConnection(conn);
            dbconn.Open();
            return dbconn;
        }
    }
}
