﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using FullCoreRepository;

namespace FullCoreWebApi.Respository
{
    public class OracleRespository
    {
        public dynamic OracleQuery(string connkey, string sql) 
        {
            using (var conn= DapperFactory.CreateOracleConnection(connkey))
            {
                return conn.Query<dynamic>(sql);
            }
        }
    }
}
