﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullCoreRepository
{
    public  class LogerHelper
    {
        private static ILogger log;

        public static ILogger GetLog()
        {
            if (log == null)
            {
                log = LogManager.GetCurrentClassLogger();
            }
            return log;
        }
    }
}
