﻿using Dapper;
using FullCoreRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullCoreWebApi.Respository
{
    public class PgsqlRespository
    {
        public dynamic PgsqlQuery(string key,string sql) 
        {
            using (var conn= DapperFactory.CreatePgsqlConnection(key))
            {
                return conn.Query<dynamic>(sql);
            }
        }
    }
}
