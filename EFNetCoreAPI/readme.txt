https://learn.microsoft.com/zh-cn/ef/core/dbcontext-configuration/
---ef连接oracle---
Microsoft.EntityFrameworkCore(ef core 核心包) *
Microsoft.EntityFrameworkCore.Relational(ef core 映射关系)
Microsoft.EntityFrameworkCore.Tools(ef core 数据迁移包) *
Oracle.EntityFrameworkCore(ef core 连接oracle) *

程序包管理控制台
Add-Migration init  --添加一次迁移记录 名字为init（自动生成实体修改后的映射）

Update-DataBase  --迁移记录保存进数据库

数据库这个表保存了迁移历史记录 __EFMigrationsHistory

