﻿using FullCoreDBModels;
using FullCoreWebApi.Respository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullCoreControllers
{
    /// <summary>
    /// 测试模块
    /// </summary>
    
    public class MyTestController : APIControllerRoot
    {

       
        public MyTestController(PatientInfoRepository PatientInfoRespository)
        {
            piRespository = PatientInfoRespository;
        }

        PatientInfoRepository piRespository;


        /// <summary>
        /// 通过日期获取数据 yfc
        /// </summary>
        /// <param date=""> 输入日期</param>
        /// <returns></returns>
        [HttpGet]
        public List<PatientInfo> GetDataByDate(string date)
        {
            return piRespository.GetPatientInfoList();
        }


        /// <summary>
        /// 通过id获取病数据 yfc
        /// </summary>
        /// <param parms=""></param>
        /// <returns></returns>
        [HttpGet]
       
        public PatientInfo GetData(string parms)
        {
            return piRespository.GetPatientInfo(parms);
        }
       
    }
}
