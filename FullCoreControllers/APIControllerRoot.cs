﻿using FullCoreDBModels;
using FullCoreWebApi.Respository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullCoreControllers
{
    /// <summary>
    /// API控制器父类 [Authorize]
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
   
    public class APIControllerRoot : ControllerBase
    {
    }
}
