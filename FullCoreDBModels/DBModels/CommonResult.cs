﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullCoreDBModels
{
    public class CommonResult
    {  /// <summary>
       /// 结果
       /// </summary>
        public bool Result { get; set; } = true;

        /// <summary>
        /// 结果相关msg
        /// </summary>
        public string ErrMsg { get; set; }

        public object ReturnData { get; set; }
    }
}
