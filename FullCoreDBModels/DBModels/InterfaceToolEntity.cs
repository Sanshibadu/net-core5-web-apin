﻿using System;

namespace FullCoreDBModels
{
    public class InterfaceToolEntity
    {
        public int ISDELETE { get; set; }
        public string ID { get; set; }
        public string INTERFACECODE { get; set; }
        public string INTERFACENAME { get; set; }
        public decimal? INVALIDFLAG { get; set; }
        public string INTERFACEDESCRIPTION { get; set; }
        public DateTime? CREATEDATE { get; set; }
        public string CREATEUSER { get; set; }
        public DateTime? UPDATEDATE { get; set; }
        public string UPDATEUSER { get; set; }
        public string INJSON { get; set; }
        public string JAVASCRIPTCODE { get; set; }
        public string OUTJSON { get; set; }
        public string CLASSNAME { get; set; }
        public string FUNCTIONNAME { get; set; }
        /// <summary>
        /// 0:javascript 1:python
        /// </summary>
        public int SCRIPTTYPE { get; set; }
    }
}