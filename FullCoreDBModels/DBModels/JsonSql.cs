﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullCoreDBModels
{
    public class JsonSql
    {
        public string SqlKey { get; set; }
        public string SqlValue { get; set; }
    }
}
