﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullCoreDBModels
{
    public class ScriptContent
    {
        /// <summary>
        /// 入参
        /// </summary>
        public string Data { get; set; }
        /// <summary>
        /// js代码内容
        /// </summary>
        public string JavaScriptCode { get; set; }
        /// <summary>
        /// js代码里的入口函数
        /// </summary>
        public string FunctionName { get; set; }
    }
}
