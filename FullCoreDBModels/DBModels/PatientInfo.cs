﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FullCoreDBModels
{
    public class PatientInfo
    {
        #region 基本信息加字段
        public string PROVINCE { get; set; }
        public string CITY { get; set; }
        public string AREA { get; set; }
        public string STREET { get; set; }
        public string COMMUNITY { get; set; }
        public string ADRESS_DETAIL { get; set; }
        #endregion
        public string PVID { get; set; }
        public string VISITID { get; set; }
        public string PATIENTID { get; set; }
        public string PATIENTNAME { get; set; }
        public string SEX { get; set; }
        public string BIRTHDATE { get; set; }
        public string ADDRESS { get; set; }
        public string CONTACTPERSON { get; set; }
        public string CONTACTPHONE { get; set; }
        public string REGISTERDT { get; set; }
        public int? STATUS { get; set; }
        public string REGISTERFROM { get; set; }
        public string VISITDATE { get; set; }
        public string IDENTITY { get; set; }
        public string CHARGETYPE { get; set; }
        public string UPDATESIGN { get; set; }
        public string REGISTERNO { get; set; }
        public string INDENTITYNO { get; set; }
        public string NATION { get; set; }
        public string COUNTRY { get; set; }
        public string PHOTO { get; set; }
        public string ORGANIZATION { get; set; }
        public string GREENROAD { get; set; }
        public string SPECIALSIGN { get; set; }
        public string BULKINJURYID { get; set; }
        public string BEDNO { get; set; }
        public string HAPPENDATE { get; set; }
        public string ISBACKNUM { get; set; }
        public string IDENTITYTYPETAG { get; set; }
        public string PREHOSPITALID { get; set; }
        public string RFID { get; set; }
        public string ADDITIONAL1 { get; set; }
        public string ADDITIONAL2 { get; set; }
        public string ADDITIONAL3 { get; set; }
        public string ADDITIONAL4 { get; set; }
        public string ADDITIONAL5 { get; set; }
        public string ADDITIONAL6 { get; set; }
        public string ADDITIONAL7 { get; set; }
        public string ADDITIONAL8 { get; set; }
        public string ADDITIONAL9 { get; set; }
        public string ADDITIONAL10 { get; set; }

        public string MAINCHIEF { get; set; }
        public string CARDNO { get; set; }
        public string AGE { get; set; }
        public string HISCLINICNO { get; set; }
        public string REGISTERUSERNAME { get; set; }
        public string REGISTERDEPTNAME { get; set; }
        public string CARDTYPE { get; set; }
        public string HOSPITALCOMPOUNDCODE { get; set; }

        public string ID { get; set; }
        public string WARDAREA { get; set; }
        public string FSTTREATCODE { get; set; }
        public string FSTTREATNAME { get; set; }
        public string DUTYDOCCODE { get; set; }
        public string DUTYDOCNAME { get; set; }
        public string LOCKDATE { get; set; }
        public string CLINICTYPE { get; set; }
        public string NURSECODE { get; set; }
        public string NURSENAME { get; set; }
        public string DOCRECEIPTDT { get; set; }
        public string ENDTREATDT { get; set; }
        public string INHOUSESTATUS { get; set; }
        public string ISPLANBACKWARD { get; set; }
        public string EMERGENCYLEVEL { get; set; }
        public string INDEPTTIME { get; set; }
        public string OUTDEPTTIME { get; set; }
        public string OPERATORCODE { get; set; }
        public string OPERATORNAME { get; set; }
        public string ISATTENTION { get; set; }
        public string DEPTNAME { get; set; }
        public string DEPTCODE { get; set; }
        public string INHOSPITALAPPLYSTATUS { get; set; }
        public string IRRITABILITY { get; set; }
        public decimal? WEIGHT { get; set; }
        public string HEIGHT { get; set; }
        public string PASTHISTORY { get; set; }
        public string ANNOUNCEMENTS { get; set; }
        /// <summary>
        /// 婚姻
        /// </summary>
        public string MARRIAGETYPE { get; set; }
        /// <summary>
        /// 职业
        /// </summary>
        public string OCCUPATION { get; set; }
        public string SPECIALMARK { get; set; }
        public string CRITICALLEVEL { get; set; }
        public string INFECTIOUSHISTORY { get; set; }
        public string CHILDWARDAREACODE { get; set; }
        public string CHILDWARDAREANAME { get; set; }

        public string TID { get; set; }
        public string TRIAGEDT { get; set; }
        public string TRIAGEBY { get; set; }
        public string TRIAGETARGET { get; set; }
        public string OTHERTRIAGETARGET { get; set; }
        public string ACTTRIAGELEVEL { get; set; }
        public string AUTOTRIAGELEVEL { get; set; }
        public string TRIAGEMEMO { get; set; }
        public string HASVITALSIGN { get; set; }
        public string HASSCORERECORD { get; set; }
        public string HASACCORDINGRECORD { get; set; }
        public string STARTRECORDDT { get; set; }
        public string REGISTERFIRST { get; set; }
        public string CHANGELEVEL { get; set; }
        public string TRIAGETARGETCODE { get; set; }
        public string OTHERTRIAGETARGETCODE { get; set; }
        /// <summary>
        /// 是否过敏 0否 1是 2未查
        /// </summary>
        public int ISALLERGY { get; set; }
        public string BEDNAME { get; set; }
        /// <summary>
        /// 就诊卡号
        /// </summary>
        public string BRKH { get; set; }

        public int? ILLNESSSTATE { get; set; }
        public string VISITNO { get; set; }
        /// <summary>
        /// 门诊号码
        /// </summary>
        public string MZHM { get; set; }

        public DateTime? PULL_INTO_BED_DT { get; set; }
        /// <summary>
        /// 最后一条就诊历史记录时间
        /// </summary>
        public DateTime? HISVISITDATE { get; set; }

        /// <summary>
        /// 就诊类别
        /// </summary>
        public string JZLB { get; set; }

        /// <summary>
        /// 电子病历状态:0未包含审签 1已有审签病历
        /// </summary>
        public int FILEINDEXSTATE { get; set; }

        /// <summary>
        /// 申请入院状态:-1:无入院数据 0:新建 1 没床等待 2有床分配 3 不入院 4 延后等待 5等待审批  9已入院 10 已出院 
        /// </summary>
        public int TOHOSPITAlSTATE { get; set; }
        public string DIAGNOSENAME { get; set; }
        /// <summary>
        /// 是否陪护：0否 1是
        /// </summary>
        public int? ISNUCDETECTION { get; set; }
        /// <summary>
        /// 院前号码
        /// </summary>
        public string YQHM { get; set; }

        /// <summary>
        /// 标题栏显示名称
        /// </summary>
        public string TITLENAME { get; set; }

        /// <summary>
        /// 五大中心标志（多个逗号间隔）1：医院胸痛中心 2：卒中中心 3：创伤救治中心 4：危重孕产妇救治中心 5：危重新生儿救治中心
        /// </summary>
        public string FIVECENTER { get; set; }
        public int? FIVECENTERSTATE { get; set; }
        public int? CENTERDATAFROM { get; set; } = 0;

        /// <summary>
        /// 操作人编码（工号）
        /// </summary>
         

        /// <summary>
        /// 叫号状态：0未叫号 1已叫号
        /// </summary>
        public int? CALLNOSTATES { get; set; }

        /// <summary>
        /// 绿色通道状态 0正常 1结束
        /// </summary>
        public int GREENROADSTATE { get; set; }

        public string NATIVEPLACE { get; set; }

    }
}
