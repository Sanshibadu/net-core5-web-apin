﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FullCoreDBModels
{
    public enum JudgmentEnum
    {
        /// <summary>
        /// 大于
        /// </summary>
        GreaterThan = 0,

        /// <summary>
        /// 大于等于
        /// </summary>
        EqualOrGreaterThan = 1,

        /// <summary>
        /// 小于
        /// </summary>
        LessThan = 2,

        /// <summary>
        /// 小于等于
        /// </summary>
        EqualOrLessThan = 3,

        /// <summary>
        /// 等于
        /// </summary>
        Equal = 4,

        /// <summary>
        /// 不等于
        /// </summary>
        UnEqual = 5,

        /// <summary>
        /// 包含
        /// </summary>
        In = 6,

        /// <summary>
        /// 不包含
        /// </summary>
        NotIn = 7,

        /// <summary>
        /// 左模糊
        /// </summary>
        LikeLeft = 8,

        /// <summary>
        /// 又模糊
        /// </summary>
        LikeRight = 9,

        /// <summary>
        /// 全模糊
        /// </summary>
        LikeAll = 10
    }
    public enum ConnectorEnum
    {
        And = 0,
        Or = 1
    }
    public class ConditionArgs
    {
        /// <summary>
        /// 前置连接符
        /// </summary>
        [DataMember]
        public ConnectorEnum PreConnector { get; set; }

        /// <summary>
        /// 中间连接符（针对多字段过滤同一个值的情况）
        /// 如 And (A = 1 OR B = 1 OR C = 1）
        /// </summary>
        [DataMember]
        public ConnectorEnum MidConnector { get; set; }

        /// <summary>
        /// 键值（一般是列名）
        /// </summary>
        [DataMember]
        public object Key { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        [DataMember]
        public object Value { get; set; }

        /// <summary>
        /// 判断符
        /// </summary>
        [DataMember]
        public JudgmentEnum JudgeType { get; set; }
    }
}
