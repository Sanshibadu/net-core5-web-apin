﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EFDBModels
{
    [Table("TRIAGEPATIENTVISIT")]
    public class TriagePatientVisit
    {
        [Key]
        [Column("PVID")]
        public string PVID { get; set; }
        
        [Column("NUM")]
        [MaxLength(15)]
        public int NUM { get; set; }
        [Column("BIRTHDATE")]
        public DateTime BIRTHDATE { get; set; }
    }
}
