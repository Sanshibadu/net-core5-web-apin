﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EFDBModels
{
    [Table("STUDENT")]  //指定数据库对应表名
    public class Student
    {
        //指定数据库表名和栏位名为大小，否则我们使用PL/SQL进行查询的时候就要加入引号，因为PL/SQL以及一些工具是不分大小写，使用起来很不方便。
        /// <summary>
        /// 学生学号
        /// </summary>
        [Key]  //主键
        [Column("USERID")] //指定数据库对应表栏位名称
        public string UserId { get; set; }

        /// <summary>
        /// 学生姓名
        /// </summary>
        [MaxLength()]
        [Column("NAME")]
        public string Name { get; set; }

        /// <summary>
        /// 学生性别
        /// </summary>
        [MaxLength()]
        [Column("SEX")]
        public string Sex { get; set; }
    }

}
