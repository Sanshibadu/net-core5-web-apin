﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;

namespace EFDBModels
{
    public static class Context
    {

        public static List<Type> GteTypes()
        {
            Func<System.Attribute[], bool> IsAtt1 = o =>
            {
                foreach (System.Attribute a in o)
                {
                    if (a is TableAttribute)
                        return true;
                }
                return false;
            };
            //当前运行程序集
            List<Type> tplist = System.Reflection.Assembly.GetExecutingAssembly().GetTypes().
                Where(o => o.IsClass && !o.IsAbstract && !o.IsGenericType && IsAtt1(Attribute.GetCustomAttributes(o, true))).Select(s => s).ToList();
            return tplist;
        }
    }
}
